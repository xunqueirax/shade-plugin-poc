package org.xunqueirax.shadePoc;

/**
 * Says hello
 */
public class SayHello {
	public String salute() {
		return "Hello!";
	}
	public String differentSalute() {
		return "Hallo!";
	}
}
