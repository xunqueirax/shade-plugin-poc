#!/bin/bash

find . -name dependency-reduced-pom.xml -exec rm {} \;

ls -d shade-plugin-poc* | xargs -n 1 mvn clean -f

exit 0
