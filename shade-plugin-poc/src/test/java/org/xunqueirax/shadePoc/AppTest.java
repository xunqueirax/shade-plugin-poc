package org.xunqueirax.shadePoc;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AppTest {

	private static String SHADED = "shaded";

	@Test
	void getHello() {
		assertTrue(App.getHello().contains(SHADED));
	}

	@Test
	void getBye() {
		assertFalse(App.getBye().contains(SHADED));
	}

	@Test
	void getEncouragement() {
		assertFalse(App.getBye().contains(SHADED));
	}

	@Test
	void getCommand() {
		assertTrue(App.getHello().contains(SHADED));
	}
}