package org.xunqueirax.shadePoc;

/**
 * Hello world!
 */
public class App {
	public static void main(String[] args) {
		System.out.println(getHello());
		System.out.println(getEncouragement());
		System.out.println(getCommand());
		System.out.println(getBye());
	}

	static String getHello() {
		return new SayHello().salute();
	}
	static String getBye() {
		return new SayBye().salute();
	}
	static String getEncouragement() {
		return new Encourage().salute();
	}
	static String getCommand() {
		return new Command().salute();
	}

}
