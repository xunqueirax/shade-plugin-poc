# Shade Plugin POC

These are four jars that try to demonstrate how the maven-shade-plugin may be used to rewrite a class of a dependency. 

I needed to do this, and that's why I wrote the POC, but this is the first time I use the plugin, so I may not see some details that may not be obvious because of the simplicity of the classes involved.

The poc project wants to depend on two dependecies dep and dep-2, and wants to rewrite a class for each. 

While for dep, it tries to rewrite the class in the self-same poc project, for dep-2 it uses the rewritten class in a dep-2-shaded jar.

As the very simple tests seem to assess, both approaches seem to work. Probably rewriting the class in a separate project is more tidy, but it has the overhead of creating a new artifact and one may not want to do that in certain circumstances.

The analysis of the contents of the jars generated make me think there is nothing fishy, but as I said, in the real world one may have more challenges.

Based on the plugin maven output, that warned me there were overlapping resources, none of them for that classes that I wanted to rewrite, I included the excludes for the META-INF files. 